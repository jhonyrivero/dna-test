<?php
namespace rivero\sample;

/** 
* Modifica un estring cambiando las letras recibidas en su metodo build por las letras subsiguientes.
* Problema 01
*/
class ChangeString
{
	
    private function isLetter($character)
	{
        return preg_match('/^[A-z]+$/', $character);
    }
	
    private function getNextCharacter($character)
	{

        switch ($character) {
        case "z":
            return "a";
        case "Z":
            return "A";
        default:
            $ascii = ord($character);
            return chr($ascii + 1);
       } 
    }

    public function build($originalString)
	{
       $result = "";
       $stringArray = str_split($originalString);
       foreach ($stringArray as $key => $value)
	   {
           if ($this->isLetter($value)) {
               $newCharacter = $this->getNextCharacter($value);
               $result .= $newCharacter;
           } else {
               $result .= $value;
           }
       }
       return $result;
    }
}

$stringChanger = new ChangeString();
$result = $stringChanger->build("123 abcd*3");
echo($result);
?>