<?php
namespace rivero\sample;
/** 
* Permite completar los numeros de un rango.
*/
class CompleteRange
{
    public function build($numbers)
    {
        return range(min($numbers),max($numbers));
    }
}

$myRange = new CompleteRange();
$inputArray = array(2, 4, 9);
$completeArray = $myRange->build($inputArray);
echo (implode(",", $completeArray));

?>