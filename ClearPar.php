<?php
namespace rivero\sample;

class ClearPar
{
    public function build($parenthesesString)
    {
        $stack = array();
        $parenthesesArray = str_split($parenthesesString);
        foreach ($parenthesesArray as $key => $value) {
			//echo ($key . "\n");
            switch ($value)
            {
                case '(':
                    array_push($stack, $key);
                    break;
                case ')':
				//$index = empty($stack) ? -1 : array_pop($stack);
				if(!empty($stack)){
					array_pop($stack);
				}else{
					array_push($stack, $key);
				}
                    break;
                default:
                    break;
            }
        }
        
        if (!empty($stack))
        {
            foreach ($stack as $position)
            {
                $parenthesesArray[$position] = "";
            }
        }
        return implode($parenthesesArray);
    }
}

$parenthesesFixer = new ClearPar();
$result = $parenthesesFixer->build("()())()");
echo ($result);

?>